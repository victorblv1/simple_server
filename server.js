const http = require('http');

const html = `
  <!doctype>
  <html>
    <head>
      <meta charset="utf-8">
      <title>Simple Node.js Server</title>
      <link rel="stylesheet" href="app.css">
    </head>
    <body>
      <h1>Simple Node.js Server</h1>
      <button>Click here</button>
      
      <script src="app.js"></script>
    </body>
  </html>
  `;

const css = `
        body {
          margin: 0;
          padding: 0;
          text-align: center;
        }
        
        h1 {
        background-color: #43853d;
        color: wheat;
        padding: .5em;
        font-family: 'Consolas';
        }
        `;

const js = `
        const button = document.querySelector('button');
        button.addEventListener('click', event => alert('Node.js in action'));
`;

http.createServer((req, res) => {
  // Return request data into CL.
  console.log(req.url);
  console.log(req.method);
  console.log(req.headers);

  switch (req.url) {
    case '/':
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.end(html);
      break;

    case '/app.css':
      res.writeHead(200, {'Content-Type': 'text/css'});
      res.end(css);
      break;

    case '/app.js':
      res.writeHead(200, {'Content-Type': 'text/js'});
      res.end(js);
      break;

    default:
      res.writeHead(404, {'Content-Type': 'text/plain'});
      res.end('404 Not Found');
      break;
  }

}).listen(3000, () => console.log('Server is running!'));